<?php

namespace App\DataFixtures;

use App\Entity\Categories;
use App\Entity\Photos;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    private $encoder;
    
    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 10; $i ++) {
            $user = new User();
            $user->setEmail($faker->email())
                ->setFirstname($faker->firstName())
                ->setLastname($faker->lastName())
                ->setRoles($faker->randomElements(['ROLE_INSTRUCTEUR', 'ROLE_APPRENANT'], $count = 1, $allowDuplicates = false))
                ->setInstagram($faker->url())
                ->setTwitter($faker->url());
            $password = $this->encoder->hashPassword($user, 'password');
            $user->setPassword($password);
            $manager->persist($user);
        }
        $user = new User();
            $user->setEmail('admin@admin.com')
                ->setFirstname('Admin')
                ->setLastname('Admin')
                ->setRoles(['ROLE_ADMIN'])
                ->setInstagram('DKAO')
                ->setTwitter('DKAO');
            $password = $this->encoder->hashPassword($user, 'admin');
            $user->setPassword($password);
            $manager->persist($user);
        $user = new User();
        $user->setEmail('user@user.com')
            ->setFirstname('User')
            ->setLastname('User')
            ->setRoles(['ROLE_User'])
            ->setInstagram('DKAO')
            ->setTwitter('DKAO');
        $password = $this->encoder->hashPassword($user, 'admin');
        $user->setPassword($password);
        $manager->persist($user);

        for ($k=0; $k < 5; $k++) {
            $categorie = new Categories();

            $categorie->setName($faker->word());
            $manager->persist($categorie);
        }

        for ($i=0; $i < 30; $i++) {
            $photo = new Photos();

            $photo->setName($faker->words(3, true))
                  ->setRealisationDate($faker->dateTimeBetween('-6 month', 'now'))
                  ->setPostDate($faker->dateTimeBetween('-3 month', 'now'))
                  ->setDescription($faker->text(350))
                  ->setPortfolio($faker->randomElement([true, false]))
                  ->setSlug($faker->slug())
                  ->setCategory($categorie)
                  ->setFile($faker->randomElement(['photo.jpg', 'flower.jpg', 'forest.jpg', 'mountain.jpg', 'road.jpg', 'russell.jpg']));
            $manager->persist($photo);
        }
        
        $manager->flush();
    }
}
